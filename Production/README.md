1. Нам понадобится предварительно подготовленный Ubuntu Server:  
  [Локальный Ubuntu Server](https://github.com/NicoFreeman/webdev-notes/tree/master/Linux/Ubuntu/Setup%20new%20Ubuntu%20Server)  
  [VPS Ubuntu Server](https://github.com/NicoFreeman/webdev-notes/tree/master/Linux/Ubuntu/VPS%20ubuntu%2014.04)  
2. Подготовка SSH
  * На локальной машине генерируем ключ(если еще не генерировали его):  
	`ssh-keygen -t rsa`
  * Передаём публичный ключ на сервер командой:  
	`ssh-copy-id -i ~/.ssh/id_rsa.pub user@server`
  * Когда ssh работает на нестандартном порту:  
	`ssh-copy-id -i ~/.ssh/id_rsa.pub -p PORT user@server`
    ```
3. Заходим по ssh на сервер: `ssh server-ip`
