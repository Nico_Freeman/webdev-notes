sudo apt-get install tcl8.5
wget http://download.redis.io/redis-stable.tar.gz
tar xvzf redis-stable.tar.gz
cd redis-stable
make
make test
sudo cp src/redis-server /usr/local/bin
sudo cp src/redis-cli /usr/local/bin
mkdir /var/www/other
mkdir /var/www/log
sudo mkdir /etc/redis
nano redis.conf
  * requirepass password
  * dir /var/www/other
sudo cp redis.conf /etc/redis/redis.conf 
sudo nano /etc/init/redis-server.conf
```  
#!upstart  
description "Redis Server"  
env USER=deployer  
start on runlevel [2345]  
stop on runlevel [016]  
respawn  
exec start-stop-daemon --start --make-pidfile --pidfile /var/run/redis-server.pid --chuid $USER --exec /usr/local/bin/redis-server /etc/redis/redis.conf >> /var/www/log/redis.log 2>&1  
```
Теперь мы можем управлять Redisом коммандами
sudo start/stop/restart/status redis-server

Возникшие проблемы:
1. Executing test client: attach_to_replication_stream error
  * Причина: selinux блокирует 11111 порт
  * Решение: `sudo sh -c 'echo "SELINUX=disabled" >> /etc/selinux/config'`  
