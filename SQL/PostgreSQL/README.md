sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'

wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get -y upgrade
sudo apt-get -y install libpq-dev
sudo apt-get -y install postgresql-9.5


#Настройка

И создаем нового пользователя: 
sudo -u postgres psql
  create user deployer with password 'ваш пароль';
  alter role deployer superuser createrole createdb replication;
  \q

Есть три файла конфигурации:

/etc/postgresql/9.5/main/postgresql.conf
#Основной файл конфигурации
http://wiki.dieg.info/postgresql.conf
http://postgresql.leopard.in.ua/html/#настройка-сервера


/etc/postgresql/9.5/main/pg_hba.conf
#Отвечает за аутентификацию системных пользователей и доступ к базам данных
Инструкция по настройке: http://postgresql-lab.blogspot.ru/2013/01/191-pghbaconf.html

  # Database administrative login by Unix domain socket
  local   all             postgres                                peer
  # TYPE  DATABASE        USER            ADDRESS                 METHOD
  host    all             railshellopg    all                     md5



/etc/postgresql/9.5/main/pg_ident.conf
#Связывает системного пользователя с пользователем БД

# MAPNAME       SYSTEM-USERNAME         PG-USERNAME
MAP             deployer                USERNAME





#on local machine for remote connect
#psql -h PostgreSQL-IP-ADDRESS -U USERNAME -d DATABASENAME
#psql -h 192.168.1.5 -U deployer -d testdb

#####################
rails new testapp -d postgresql
cd testapp
nano config/database.yml
  host: localhost
  username: pguser
  password: pguser_password








gem install pg

Теперь создайте пользователя Postgres для приложения Rails, которое будет создано далее. Для этого откройте стандартного пользователя Postgres:

su - deployer

и создайте нового пользователя (или т.н. «роль», согласно терминологии Postgres):

create role myapp with createdb login password 'password1'

Чтобы создать приложение Rails для Postgres, запустите команду:

rails new myapp --database=postgresql

Эта команда создаст каталог по имени myapp, который содержит приложение по имени myapp (это имя используется в руководстве для простоты примера; конечно, назвать приложение можно любым удобным именем). Примечание: Rails подразумевает использование одного имени для пользователя БД и приложения, но это условие необязательно выполнять. 
Теперь нужно настроить взаимодействие Rails с базой данных. Это делается при помощи файла database.yml, который находится в:

RAILS_ROOT/config/database.yml

Примечание: как видно из названия, RAILS_ROOT – это root-каталог Rails. Он находится в каталоге приложения (согласно данному руководству, это каталог /myapp).

Файл database.yml используется Rails для подключения к соответствующей БД для текущего окружения Rails. Этот файл разработан согласно стандарту сериализации данных YAML. Файл содержит несколько БД для разных окружений: для разработки, тестирования и производства. По умолчанию Rails предполагает использование отдельной БД для каждого этапа. Это удобно, поскольку БД для определенного этапа ведет себя соответствующим образом (например, БД для тестирования перестраивается для каждого тестирования Rails). Каждая БД по умолчанию должна использовать имя и пароль пользователя Postgres.

Готовый файл database.yml должен выглядеть примерно так:

development:
adapter: postgresql
encoding: unicode
database: myapp_development
pool: 5
username: myapp
password: password1
test:
adapter: postgresql
encoding: unicode
database: myapp_test
pool: 5
username: myapp
password: password1

Затем нужно запустить:

rake db:setup

Это создаст базы данных для разработки и тестирования, передаст права на них указанному в файле пользователю Postgres, а также создаст таблицы schema_migrations дл каждой БД (эти таблицы используются для записи миграций).








