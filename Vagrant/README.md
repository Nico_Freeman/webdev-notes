1. Установка:
  * Скачиваем .deb пакет со страницы загрузки https://www.vagrantup.com/downloads.html:  
  `wget https://releases.hashicorp.com/vagrant/1.8.1/vagrant_1.8.1_x86_64.deb`
  * Устанавливаем Vagrant:  
  `sudo dpkg -i vagrant_1.8.1_x86_64.deb`
5. Тестируем vagrant:  
  `mkdir vagrant_getting_started`  
  `cd vagrant_getting_started`  
  `vagrant init`  


3. Устанавливаем VirtualBox:
  * Download:`sudo apt-get install virtualbox`
  * После того как VirtualBox установится, вам нужно добавить вашего пользователя в группу vboxusers:  
  `sudo usermod -a -G vboxusers "whoami"`
  * Перезагружаем сервер:`reboot`
4. Install Vagrant:
  * Скачиваем .deb пакет со страницы загрузки https://www.vagrantup.com/downloads.html:  
  `wget https://releases.hashicorp.com/vagrant/1.8.1/vagrant_1.8.1_x86_64.deb`
  * Устанавливаем Vagrant:  
  `sudo dpkg -i vagrant_1.8.1_x86_64.deb`
5. Тестируем vagrant:  
  `mkdir vagrant_getting_started`  
  `cd vagrant_getting_started`  
  `vagrant init`  
