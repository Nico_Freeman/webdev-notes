rails new goingtofreedom -d postgresql
cd goingtofreedom
bundle install

sudo -u postgres psql
  create user goingtofreedom with password 'secret';
  alter role goingtofreedom superuser createrole createdb replication;
  \q

sudo bash -c "echo 'host    goingtofreedom_development             goingtofreedom    all                     md5' >> /etc/postgresql/9.5/main/pg_hba.conf"

#Modify config/database.yml
  development:
    database: goingtofreedom_development
    adapter: postgresql
    encoding: unicode
    pool: 5
    username: goingtofreedom
    password: secret
    host: localhost
    port: 5432


rake db:create

