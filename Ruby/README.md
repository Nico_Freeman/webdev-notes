1. Устанавливаем git, curl, python, nodejs  
```
sudo add-apt-repository ppa:chris-lea/node.js  

sudo apt-get update  
sudo apt-get -y upgrade
sudo apt-get -y install git python-software-properties build-essential libssl-dev libyaml-dev libreadline-dev openssl curl git-core zlib1g-dev bison libxml2-dev libxslt1-dev libcurl4-openssl-dev nodejs libsqlite3-dev sqlite3  
```
2. Install RVM stable with ruby & requirements packages  
```
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3  
\curl -sSL https://get.rvm.io | bash -s stable --ruby  
source ~/.rvm/scripts/rvm  
rvm requirements  
```
3. `sudo bash -c "echo 'gem: --no-rdoc --no-ri' >> ~/.gemrc"`
4. Устанавливаем rails & bundler  
```
gem install rails bundler
```
5. ????????  Устанавливаем и настраиваем Bundler, чтобы он хранил все гемы в директори проекта:  
```  
bundle config --global path vendor/bundle  
bundle config --global bin vendor/bundle/bin  
```  
