# Passenger & Nginx


###########################
###Compiling from source

sudo apt-get -y install libpcre3-dev build-essential libssl-dev
sudo mkdir /opt
sudo mkdir /opt/nginx
sudo chown -R deployer /opt/nginx
mkdir /opt/nginx/src
mkdir /opt/nginx/cache
mkdir /opt/nginx/sites-available
mkdir /opt/nginx/sites-enabled
cd /opt/nginx/src
wget http://nginx.org/download/nginx-1.11.0.tar.gz
wget http://zlib.net/zlib-1.2.8.tar.gz
tar xfz nginx-1.11.0.tar.gz && tar xfz zlib-1.2.8.tar.gz
gem install passenger


При компиляции имеет смысл указывать следующие блоки:
rvmsudo passenger-install-nginx-module --auto --nginx-source-dir=/opt/nginx/src/nginx-1.11.0 --extra-configure-flags="--prefix=/opt/nginx --sbin-path=/opt/nginx/sbin/nginx --conf-path=/opt/nginx/nginx.conf --error-log-path=/opt/nginx/log/error.log --http-log-path=/opt/nginx/log/access.log --pid-path=/opt/nginx/run/nginx.pid --lock-path=/opt/nginx/run/nginx.lock --http-client-body-temp-path=/opt/nginx/cache/client_temp --http-proxy-temp-path=/opt/nginx/cache/proxy_temp --http-fastcgi-temp-path=/opt/nginx/cache/fastcgi_temp --http-uwsgi-temp-path=/opt/nginx/cache/uwsgi_temp --http-scgi-temp-path=/opt/nginx/cache/scgi_temp --user=deployer --group=deployer --with-zlib='/opt/nginx/src/zlib-1.2.8' --without-http_fastcgi_module --without-http_uwsgi_module --with-http_stub_status_module --with-http_gzip_static_module"


###Copy/download/curl/wget the init script
sudo wget https://raw.githubusercontent.com/JasonGiedymin/nginx-init-ubuntu/master/nginx -O /etc/init.d/nginx
sudo nano /etc/init.d/nginx

sudo chmod +x /etc/init.d/nginx
sudo update-rc.d -f nginx defaults

###########################
###Install via APT

#Install PGP key and add HTTPS support for APT
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 561F9B9CAC40B2F7
sudo apt-get install -y apt-transport-https ca-certificates

# Add our APT repository
sudo sh -c 'echo deb https://oss-binaries.phusionpassenger.com/apt/passenger trusty main > /etc/apt/sources.list.d/passenger.list'
sudo apt-get update

# Install Passenger + Nginx
sudo apt-get install -y nginx-extras passenger
###########################



2. Configure Nginx  

Настраиваем по аналогии с https://gist.github.com/NicoFreeman/c28ef9c10cd9bd37bcfc0e0e86a6d560
sudo nano /opt/nginx/nginx.conf

Настраиваем по аналогии с https://gist.github.com/NicoFreeman/12153ff31e93b16ccefac0e356e4e80d
nano /opt/nginx/sites-available/testapp.conf
ln -s /opt/nginx/sites-available/testapp.conf /opt/nginx/sites-enabled/testapp.conf




/opt/www/testapp/access.log 
/opt/www/testapp/index.html 



/var/log/nginx/error.log
/var/log/nginx/access.log
/usr/share/nginx/html/index.html 




sudo mkdir /opt/www
sudo chgrp -R deployer www
sudo chmod -R g+rwxs www
mkdir /opt/www/testapp.com
echo "<html><h1>Hello!</h1></html>" > /opt/www/testapp.com/index.html

sudo nano /etc/nginx/sites-available/testapp.com 
  server {
    listen 80;
    server_name testapp;
    access_log /opt/www/testapp.com/access.log;

    root /opt/www/testapp.com;

  }

sudo ln -s /etc/nginx/sites-available/testapp.com /etc/nginx/sites-enabled/testapp.com
sudo service nginx restart 
sudo nginx -s reload
sudo nginx -t
sudo nginx -T


















sudo ./configure
--prefix=/opt/nginx
--sbin-path=/opt/nginx/sbin/nginx
--conf-path=/opt/nginx/nginx.conf
--error-log-path=/opt/nginx/log/error.log
--http-log-path=/opt/nginx/log/access.log
--pid-path=/opt/nginx/run/nginx.pid
--lock-path=/opt/nginx/run/nginx.lock
--http-client-body-temp-path=/opt/nginx/cache/client_temp
--http-proxy-temp-path=/opt/nginx/cache/proxy_temp
--http-fastcgi-temp-path=/opt/nginx/cache/fastcgi_temp
--http-uwsgi-temp-path=/opt/nginx/cache/uwsgi_temp
--http-scgi-temp-path=/opt/nginx/cache/scgi_temp
--user=deployer
--group=deployer
--without-http_fastcgi_module
--without-http_uwsgi_module
--with-http_stub_status_module
--with-http_gzip_static_module
--add-module='/opt/nginx/src/passenger/src/nginx_module'





sudo ./configure
--prefix=/opt/nginx
--sbin-path=/opt/nginx/sbin/nginx
--conf-path=/opt/nginx/nginx.conf
--error-log-path=/opt/nginx/log/nginx/error.log
--http-log-path=/opt/nginx/log/nginx/access.log
--pid-path=/opt/nginx/run/nginx.pid
--lock-path=/opt/nginx/run/nginx.lock
--http-client-body-temp-path=/opt/nginx/cache/nginx/client_temp
--http-proxy-temp-path=/opt/nginx/cache/nginx/proxy_temp
--http-fastcgi-temp-path=/opt/nginx/cache/nginx/fastcgi_temp
--http-uwsgi-temp-path=/opt/nginx/cache/nginx/uwsgi_temp
--http-scgi-temp-path=/opt/nginx/cache/nginx/scgi_temp
--user=deployer
--group=deployer
--with-http_ssl_module
--with-http_realip_module
--with-http_addition_module
--with-http_sub_module
--with-http_dav_module
--with-http_flv_module
--with-http_mp4_module
--with-http_gunzip_module
--with-http_gzip_static_module
--with-http_random_index_module
--with-http_secure_link_module
--with-http_stub_status_module
--with-http_auth_request_module
--with-mail
--with-mail_ssl_module
--with-file-aio
--with-http_spdy_module
--with-cc-opt='-g -O2 -fstack-protector-strong -Wformat -Werror=format-security'
--with-ld-opt=-Wl,-z,relro
--with-ipv6



rvmsudo
passenger-install-nginx-module
--auto
--nginx-source-dir=/opt/nginx/src/nginx-1.11.0
--extra-configure-flags="--prefix=/opt/nginx
--sbin-path=/opt/nginx/sbin/nginx
--conf-path=/opt/nginx/nginx.conf
--error-log-path=/var/log/nginx/error.log
--http-log-path=/var/log/nginx/access.log
--pid-path=/var/run/nginx.pid
--lock-path=/var/run/nginx.lock
--http-client-body-temp-path=/var/cache/nginx/client_temp
--http-proxy-temp-path=/var/cache/nginx/proxy_temp
--http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp
--http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp
--http-scgi-temp-path=/var/cache/nginx/scgi_temp
--user=deployer
--group=deployer
--with-http_ssl_module
--with-http_gzip_static_module
--with-http_stub_status_module
--with-cc-opt=-Wno-error
--with-ld-opt=''
--with-pcre='/tmp/passenger.1nbnwt6/pcre-8.34'
--add-module='/home/deployer/.rvm/gems/ruby-2.3.0/gems/passenger-5.0.28/src/nginx_module'"
