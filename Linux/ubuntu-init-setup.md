1. Скачиваем Ubuntu Server 14.04 LTC:
  * [directly](http://www.ubuntu.com/download/server)
  * [torrent](http://www.ubuntu.com/download/alternative-downloads)
2. Записываем ISO:
  * [Windows](https://rufus.akeo.ie/)
  * [Ubuntu](http://www.ubuntu.com/download/desktop/create-a-usb-stick-on-ubuntu)  
3. Установка и первичная настройка:  
  * [Устанавливаем используя первую часть мануала](http://help.ubuntu.ru/fullcircle/31/server_%D1%87_1)  
4. Настраиваем сеть:  
  * Изменяем имя сервера: `$ echo server1.example.com > /etc/hostname`  
  * Правим хосты: `$ nano /etc/hosts`  
  * Прописываем статический ip: `$ nano /etc/network/interfaces`  
  ```
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).
# The loopback network interface
auto lo
iface lo inet loopback
# The primary network interface
auto eth0
iface eth0 inet static
    address 192.168.1.100
    netmask 255.255.255.0
    network 192.168.1.0
    broadcast 192.168.1.255
    gateway 192.168.1.1
    dns-nameservers 8.8.8.8
  ```
5. Добавляем пользователя deployer:
  * `$ adduser deployer`  
  * Добавляем ему права `echo "deployer ALL=(ALL:ALL) ALL" >> /etc/sudoers`  
6. Устанавливаем и настраиваем SSH
  * Устанавливаем обновления и ssh сервер:  
    `$ apt-get update`  
    `$ apt-get upgrade`  
    `$ apt-get install ssh openssh-server`  
  * Меняем настройки SSH: `nano /etc/ssh/sshd_config`  
```
Port 2222  
PermitRootLogin no  
UseDNS no  
AllowUsers deployer  
```
7. Конфигурируем фаервол:  
```  
ufw allow 2222  
ufw allow 80  
ufw allow 443  
ufw --force enable  
```
8. Make swap  
```  
dd if=/dev/zero of=/swapfile bs=1024 count=256k  
mkswap /swapfile  
swapon /swapfile  
bash -c "echo '/swapfile       none    swap    sw      0       0' >> /etc/fstab"  
sudo chown root:root /swapfile   
sudo chmod 0600 /swapfile  
```  
9. Перезапустим сервер:`init 6`
