1. Меняем пароль root: `passwd`
2. Change locale  
`nano /etc/default/locale`  
```
LANGUAGE='en_US.UTF-8'
LC_ALL='en_US.UTF-8'
LANG='en_US.UTF-8'
LC_TYPE='en_US.UTF-8'
```  
`locale-gen en_US.UTF-8`  
`dpkg-reconfigure locales`  
3. Создаем нового пользователя deployer:`adduser deployer`
4. Разрешаем ему пользоваться sudo:`visudo`
  * Дописываем следующую строку в файл:`deployer ALL=(ALL:ALL) ALL`
5. Меняем имя сервера:  
  * Изменяем имя сервера: `$ echo server1.example.com > /etc/hostname`  
  * Правим хосты: `$ nano /etc/hosts`  
6. Меняем настройки ssh:`nano /etc/ssh/sshd_config`  
```
Port 2200  
PermitRootLogin no  
UseDNS no  
AllowUsers deployer  
```
7. Make swap  
```  
dd if=/dev/zero of=/swapfile bs=1M count=1024  
mkswap /swapfile  
swapon /swapfile  
bash -c "echo '/swapfile       none    swap    sw      0       0' >> /etc/fstab"  
chown root:root /swapfile  
chmod 0600 /swapfile  
```
8. `ssh-keygen -t rsa -b 2048`

9. `init 6`
